import mongoose from "mongoose";

const Schema = mongoose.Schema;

const productSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    reviews: Array,
    lastModifiedDate: Date
});

productSchema.pre('save', function (next) {
    this.lastModifiedDate = new Date();
    next();
});

export default mongoose.model('Product', productSchema);