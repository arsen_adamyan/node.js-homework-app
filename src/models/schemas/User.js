import mongoose from "mongoose";

const Schema = mongoose.Schema;

const userSchema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: false
    },
    lastModifiedDate: Date
});

userSchema.pre('save', function (next) {
    this.lastModifiedDate = new Date();
    next();
});

export default mongoose.model('User', userSchema);