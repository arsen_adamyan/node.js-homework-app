#!/usr/bin/env node

const program = require('commander');
const action = require('./cliActions');

program
    .version('0.0.1', '-v, --version')
    .option('-a, --action <action>', 'An action')
    .option('-f, --file <filePath>', 'File path')
    .option('-p, --path <filePath>', 'CSS File path')
    .parse(process.argv);

if (typeof program.action === 'function') {
    process.stdout.write('WARNING: You should write down at least one option!\n');
    program.help();
} else {
    action(program);
}