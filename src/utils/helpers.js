export function objectIsEmpty(object) {
    return Object.keys(object).length === 0;
}