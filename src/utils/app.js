import config from "../config";
import { User } from "../models/User";
import { Product } from "../models/Product";
import path from "path";
import {DirWatcher, Importer} from "./index";

console.log(config.name);

const user = new User();
const product = new Product();
const pathToWatch = path.join(__dirname, '../data');
const dirwatcher = new DirWatcher();
const importer = new Importer();

dirwatcher.watch(pathToWatch, 3000);