import {emitter} from "../common/emitter";
import csv from "convert-csv-to-json";
import path from "path";
import {DirWatcher} from "./dirwatcher";

export class Importer {
    constructor() {
        this.dirwatcher = new DirWatcher();
        emitter.on('​dirwatcher:changed', this.import.bind(this));
    }

    importSync(dir) {
        const files = this.dirwatcher.getFilesSync(dir);
        const output = this.getFilesContent(files);

        console.log(output);
        return output;
    }

    import(files) {
        const data = this.getFilesContent(files);

        console.log(data);
        return new Promise(resolve => resolve(data));
    }

    getFilesContent(files) {
        const output = [];

        files.forEach(file => {
            const fileName = file.name;
            const filePath = path.join(file.path, fileName);
            const data = csv.getJsonFromCsv(filePath);

            output.push({
                fileName,
                data
            });
        });

        return output;
    }
}