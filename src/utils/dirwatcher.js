import fs from "fs";
import {emitter} from "../common/emitter";
import path from "path";
import util from "util";

export class DirWatcher {
    files = [];

    watch(dir, delay) {
        setInterval(() => this.checkForFiles(dir), delay);
    }

    checkForFiles(dir) {
        const files = this.getFilesSync(dir);

        if (!util.isDeepStrictEqual(files, this.files)) {
            this.files = files;
            emitter.emit('​dirwatcher:changed', files);
        }
    }

    getFilesSync(dir, fileList = []) {
        const files = fs.readdirSync(dir).filter(item => {
            const itemPath = path.join(dir, item);

            return fs.statSync(itemPath).isDirectory(itemPath) || (fs.statSync(itemPath).isFile(itemPath) && path.extname(item) === '.csv');
        });

        files.forEach(file => {
            const filePath = path.join(dir, file);

            if (fs.statSync(filePath).isDirectory(filePath)) {
                fileList = this.getFilesSync(filePath, fileList);
            } else {
                fileList.push({
                    name: file,
                    path: dir,
                    modifiedDate: fs.statSync(filePath).mtime.getTime()
                });
            }
        });

        return fileList;
    }
}