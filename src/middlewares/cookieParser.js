export default function (req, res, next) {
    const cookies = req.headers.cookie ? req.headers.cookie.split(' ') : null;
    const parsedCookies = {};

    if (cookies) {
        for (const cookie of cookies) {
            const key = cookie.substring(0, cookie.indexOf('='));
            const value = cookie.split('=')[1];

            if (value[value.length - 1] === ';') {
                value = value.slice(0, -1);
            }
            parsedCookies[key] = value;
        }
        req.parsedCookies = parsedCookies;
    }

    next();
};