import jwt from "jsonwebtoken";
import config from '../config';

export default function (req, res, next) {
    try {
        const token = req.headers['authorization'].split(' ')[1];

        jwt.verify(token, config.jwtSecret, err => {
            if (err) {
                res.sendStatus(403);
            } else {
                next();
            }
        });
    } catch (e) {
        return res.sendStatus(403);
    }
}