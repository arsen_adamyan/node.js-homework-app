import passport from "passport";
import FacebookStrategy from "passport-facebook";
import {options} from "./index";

const facebookOptions = {
    clientID: 'SOME_FACEBOOK_APP_ID',
    clientSecret: 'SOME_FACEBOOK_APP_SECRET',
    callbackURL: 'http://www.example.com/auth/facebook/callback'
};
const strategy = new FacebookStrategy(
    facebookOptions,
    function (accessToken, refreshToken, profile, done) {
        return done(null, true);
    }
);

passport.use(options.strategies.FACEBOOK, strategy);