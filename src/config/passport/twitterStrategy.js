import passport from "passport";
import TwitterStrategy from "passport-twitter";
import {options} from "./index";

const twitterOptions = {
    consumerKey: 'SOME_TWITTER_CONSUMER_KEY',
    consumerSecret: 'SOME_TWITTER_CONSUMER_SECRET',
    callbackURL: 'http://www.example.com/auth/twitter/callback'
};
const strategy = new TwitterStrategy(
    twitterOptions,
    function (token, tokenSecret, profile, done) {
        return done(null, true);
    }
);

passport.use(options.strategies.TWITTER, strategy);