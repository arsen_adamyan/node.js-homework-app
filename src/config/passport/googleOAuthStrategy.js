import passport from "passport";
import Strategy from "passport-google-oauth";
import {options} from "./index";

const GoogleStrategy = Strategy.OAuthStrategy;

const googleOptions = {
    consumerKey: 'SOME_GOOGLE_CONSUMER_KEY',
    consumerSecret: 'SOME_GOOGLE_CONSUMER_SECRET',
    callbackURL: 'http://www.example.com/auth/google/callback'
};
const strategy = new GoogleStrategy(
    googleOptions,
    function (token, tokenSecret, profile, done) {
        return done(null, true);
    }
);

passport.use(options.strategies.GOOGLE, strategy);