import config from '../';
import passportJWT from "passport-jwt";

const ExtractJWT = passportJWT.ExtractJwt;

export const options = {
    strategies: {
        LOCAL: 'local',
        JWT: 'jwt',
        FACEBOOK: 'facebook',
        TWITTER: 'twitter',
        GOOGLE: 'google'
    },
    jwtOptions: {
        secretOrKey: config.jwtSecret,
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken()
    }
};