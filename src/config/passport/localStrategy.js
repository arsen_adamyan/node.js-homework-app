import passport from "passport";
import {options} from './';
import LocalStrategy from "passport-local";

const parameters = {
    usernameField: 'email',
    passwordField: 'password'
};
const USER = {
    id: 1,
    password: 'password',
    email: 'user@mail.com',
    username: 'username'
};
const strategy = new LocalStrategy(
    parameters,
    function (email, password, done) {
        if (!checkAuth(email, password)) {
            return done(null, {});
        }

        delete USER.password;
        return done(null, USER);
    }
);

passport.use(options.strategies.LOCAL, strategy);

function checkAuth(email, password) {
    return email === USER.email && password === USER.password;
}