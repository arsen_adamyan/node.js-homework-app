import Sequelize from "sequelize";
import { development } from "../config";

const { database, username, password } = development;
const options = { dialect: 'postgres' };
const connection = new Sequelize(database, username, password, options);

connection
    .authenticate()
    .then(() => console.log('Connection has been established successfully.'))
    .catch(err => console.error('Unable to connect to the database:', err));

export default connection;