import mongoose from "mongoose";

mongoose.connect('mongodb://localhost/homework');

mongoose
    .connection
    .on('error', console.log)
    .once('open', () => console.log('Successfully connected to MongoDB'));

