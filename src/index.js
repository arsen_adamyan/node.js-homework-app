import app from './app';
const port = process.env.PORT || 8080;

app.listen(port, err => {
    if (err) return console.log('Something went wrong', err);
    console.log(`App listening on port ${port}`);
});