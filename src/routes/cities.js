import {Router} from "express";
import {getRandomCity, getAllCities, createCity, updateCity, deleteCity} from '../controllers/city';

const router = Router();

router.get('/random', getRandomCity);
router.get('/', getAllCities);
router.post('/', createCity);
router.put('/:id', updateCity);
router.delete('/:id', deleteCity);

export default router;