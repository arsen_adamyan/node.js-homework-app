import {Router} from "express";
import {getAllUsers, deleteUser} from '../controllers/user';

const router = Router();

router.get('/', getAllUsers);
router.delete('/:id', deleteUser);

export default router;