import {Router} from "express";
import {login, facebookCallback, twitterCallback, googleCallback} from "../controllers/auth";
import passport from "passport";
import {options} from "../config/passport";

const router = Router();
const {strategies} = options;

router.post('/', passport.authenticate(strategies.LOCAL, {session: false}), login);

router.get('/facebook', passport.authenticate(strategies.FACEBOOK));
router.get('/facebook/callback', passport.authenticate(strategies.FACEBOOK), facebookCallback);

router.get('/twitter', passport.authenticate(strategies.TWITTER));
router.get('/twitter/callback', passport.authenticate(strategies.TWITTER), twitterCallback);

router.get('/google', passport.authenticate(strategies.GOOGLE));
router.get('/google/callback', passport.authenticate(strategies.GOOGLE), googleCallback);

export default router;