import {Router} from "express";
import {getAllProducts, getProductById, getProductReviews, createProduct} from '../controllers/product';

const router = Router();

router.get('/', getAllProducts);
router.get('/:id', getProductById);
router.get('/:id/reviews', getProductReviews);
router.post('/', createProduct);

export default router;