import Product from '../models/schemas/Product';
import {handleError, handleValidation} from "./helpers";

exports.getAllProducts = async function (req, res) {
    try {
        const products = await Product.find();

        return res.json(products);
    } catch (err) {
        return res.send(err);
    }
};

exports.getProductById = async function (req, res) {
    const {id} = req.params;

    try {
        const product = await Product.findById(id);

        return res.json(product);
    } catch (err) {
        handleError(err, req, res);
    }
};

exports.getProductReviews = async function (req, res) {
    const {id} = req.params;

    try {
        const product = await Product.findById(id);

        const {reviews} = product;
        return res.json({reviews});
    } catch (err) {
        handleError(err, req, res);
    }
};

exports.createProduct = async function (req, res) {
    const {title, reviews} = req.body;

    try {
        const newProduct = new Product({title, reviews});
        const result = await newProduct.save();

        return res.status(201).json(result);
    } catch (err) {
        handleValidation(err, req, res);
    }
};