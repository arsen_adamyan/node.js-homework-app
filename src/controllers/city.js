import City from '../models/schemas/City'
import {handleValidation, getUpdateData} from '../controllers/helpers';
import {handleError} from "./helpers";

exports.getRandomCity = function (req, res) {
    City
        .count()
        .exec((err, count) => {
            if (err) return res.send(err);

            const random = Math.floor(Math.random() * count);

            City
                .findOne()
                .skip(random)
                .exec((err, city) => {
                    if (err) return res.send(err);

                    return res.json(city);
                });
        });
};

exports.getAllCities = function (req, res) {
    City
        .find()
        .then((err, cities) => {
            if (err) return res.send(err);

            return res.json(cities);
        });
};

exports.createCity = async function (req, res, next) {
    const {name, country, capital, location} = req.body;
    const newCity = new City({
        name,
        location,
        country,
        capital
    });

    try {
        const createdCity = await newCity.save();

        return res.status(201).json(createdCity);
    } catch (err) {
        handleValidation(err, req, res);
    }
};

exports.updateCity = async function (req, res) {
    const {id} = req.params;

    try {
        const updatedCity = await City.findByIdAndUpdate(id, req.body);

        return res.json(updatedCity);
    } catch (err) {
        handleError(err, req, res);
    }
};

exports.deleteCity = async function (req, res) {
    const {id} = req.params;

    try {
        const result = await City.findByIdAndDelete(id);

        return res.json(result);
    } catch (err) {
        handleError(err, req, res);
    }
};