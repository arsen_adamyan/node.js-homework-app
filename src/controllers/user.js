import User from '../models/schemas/User';
import {handleError} from "./helpers";

exports.getAllUsers = async function (req, res) {
    try {
        const users = await User.find();

        return res.json(users);
    } catch (err) {
        return res.send(err);
    }
};

exports.deleteUser = async function (req, res) {
    const {id} = req.params;

    try {
        const result = await User.findByIdAndDelete(id);

        return res.json(result);
    } catch (err) {
        handleError(err, req, res);
    }
};