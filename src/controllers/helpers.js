export const handleValidation = function (err, req, res, next) {
    const {errors} = err;
    const validationMessages = {};

    for (const field in errors) {
        if (errors.hasOwnProperty(field)) {
            validationMessages[field] = errors[field].message;
        }
    }

    return res.status(400).send({errors: validationMessages});
};

export const handleError = function (err, req, res, next) {
    const {path} = err;

    if (path === '_id') return res.status(404).json({
        message: "Not found"
    });
};