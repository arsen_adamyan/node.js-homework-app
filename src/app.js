import express from "express";
import * as routes from './routes';
import bodyParser from "body-parser";
import cookieParser from './middlewares/cookieParser';
import verifyToken from './middlewares/auth';
import passport from "passport";

const app = express();

// DB connection

// SQL (Sequelize)
// require('./database/connection');

// NoSQL MongoDB
require('./database/mongo/connection');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cookieParser);

// Passport configs
app.use(passport.initialize());
require('./config/passport/localStrategy');
require('./config/passport/facebookStrategy');
require('./config/passport/twitterStrategy');
require('./config/passport/googleOAuthStrategy');

app.use('/api/auth', routes.auth);
app.use('/api/users', verifyToken, routes.users);
app.use('/api/products', verifyToken, routes.products);
app.use('/api/cities', verifyToken, routes.cities);

export default app;