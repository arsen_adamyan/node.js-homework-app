# Node.js Homework application

## Installation

After cloning run
 ```bash
npm install
```

## Run application

Run development server

```bash
npm start
```
Run production build
```bash
npm run build
npm run serve
```

## License
[ISC](https://choosealicense.com/licenses/mit/)