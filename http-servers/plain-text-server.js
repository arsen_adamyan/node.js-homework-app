const http = require("http");
const server = http.createServer(requestHandler);
const { PORT } = require('./config');

function requestHandler(req, res) {
    res.writeHead(200, {
        'Content-Type': 'text/plain'
    });
    res.end('Hello, World!');
}

server.listen(PORT, err => {
    if (err) return console.log('Something went wrong.', err);
    console.log(`Server is listening on port ${PORT}`);
});