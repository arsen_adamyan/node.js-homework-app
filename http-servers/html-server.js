const http = require("http");
const server = http.createServer(requestHandler);
const { PORT } = require('./config');
const fs = require("fs");
const path = require("path");
const ReplaceStream = require('./transformStrams/replace-text');

function requestHandler(req, res) {
    const readStream = fs.createReadStream(path.join(__dirname, 'index.html'), 'utf8');
    const replaceStream = new ReplaceStream({
        message: 'Hello, World!'
    });

    res.writeHead(200, {
        'Content-Type': 'text/html'
    });
    readStream
        .pipe(replaceStream)
        .pipe(res);
}

server.listen(PORT, err => {
    if (err) return console.log('Something went wrong.', err);
    console.log(`Server is listening on port ${PORT}`);
});