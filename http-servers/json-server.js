const http = require("http");
const server = http.createServer(requestHandler);
const { PORT } = require('./config');

function requestHandler(req, res) {
    const product = {
        id: 1,
        name: 'Supreme T-Shirt',
        brand: 'Supreme',
        price: 99.99,
        options: [
            { color: 'blue' },
            { size: 'XL' }
        ]
    };

    res.writeHead(200, {
        'Content-Type': 'application/json'
    });
    res.end(JSON.stringify(product));
}

server.listen(PORT, err => {
    if (err) return console.log('Something went wrong', err);
    console.log(`Server is listening on port ${PORT}`);
});